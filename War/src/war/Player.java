/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package war;

import java.util.Random;
import java.util.Stack;

/**
 *
 * @author ejm116
 */
public class Player {
    private Stack<Card> hand;
    private static final Random rand = new Random();
    
    
    public Player(){
        hand = new Stack<>();
    }
    
    public Card draw(){
        return hand.pop();
    }
    
    public String getScore(){
        return Integer.toString(hand.size());
    }
    
    // place card at bottom of stack
    public void prepend(Card c){
        hand.add(0, c);
    }
    
    public void append(Card c){
        hand.push(c);
    }
    
    // add card c to hand at random position but not the top
    public void insertCard(Card c){
        hand.add(rand.nextInt(hand.size() - 2), c);
    }
}
