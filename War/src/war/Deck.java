/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package war;

import java.util.Stack;
import java.util.Collections;
import java.util.EmptyStackException;

/**
 *
 * @author ejm116
 */
public class Deck {
    private Stack<Card> deck;
    
    public Deck(){
        deck = new Stack<>();
        for(int j = 0; j <= 3; j++){
            for(int i = 1; i <= 13; i++){
                deck.add(new Card(i, j));
            }
        }
        for(int i = 0; i < 5; i++)
            this.shuffle();
    }
    
    public void populate(){
        for(int j = 0; j <= 3; j++){
            for(int i = 1; i <= 13; i++){
                deck.push(new Card(i, j));
            }
        }
    }
    
    public int size(){
        return deck.size();
    }
    
    public Card draw(){
        try{
            return deck.pop();
        } catch (EmptyStackException e){
            return null;
        }
    }
    
    public void shuffle(){
        Collections.shuffle(deck);
    }
    
    public void printAllCards(){
        for(Card x : deck){
            System.out.println(x);
        }
    }
}
