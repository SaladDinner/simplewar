/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package war;

/**
 *
 * @author ejm116
 */

public class Card implements Comparable{
    public final static int HEARTS = 0;
    public final static int SPADES = 1;
    public final static int CLUBS = 2;
    public final static int DIAMONDS = 3;
    private final int faceValue;
    private final int suit; 
    
    public Card(int faceValue, int suit) {
        this.suit = suit;
        this.faceValue = faceValue;
    }
    
    private String suitToString(){
        switch (suit) {
            case HEARTS:
                return "Hearts";
            case SPADES:
                return "Spades";
            case CLUBS:
                return "Clubs";
            default:
                return "Diamonds";
        }
    }
    
    public int getSuit(){ return suit; };
    public int getFaceValue() { return faceValue; };
    
    public String getImage(){
        String value;
        switch (faceValue) {
            case 1:
                value = "Ace";
                break;
            case 11:
                value = "Jack";
                break;
            case 12:
                value = "Queen";
                break;
            case 13:
                value = "King";
                break;
            default:
                value = Integer.toString(faceValue);
                break;
        }
        return String.format("/images/%s%s.png", value, suitToString());
    }
    
    @Override
    public String toString(){
        StringBuilder s = new StringBuilder();
        switch(faceValue){
            case 1:
                s.append("ace");
                break;
            case 11:
                s.append("jack");
                break;
            case 12:
                s.append("queen");
                break;
            case 13:
                s.append("king");
                break;
            default:
                s.append(faceValue);
        }
        s.append(" of ");
        s.append(suitToString());
        return s.toString();
    }

    @Override
    public int compareTo(Object o) {
        if(!(o instanceof Card)){ return -1; }
        Card c = (Card)o;
        return this.faceValue - c.faceValue;
    }
}
